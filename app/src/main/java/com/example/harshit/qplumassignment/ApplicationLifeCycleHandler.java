package com.example.harshit.qplumassignment;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;



/**
 * Details - stackoverflow.com/questions/3667022/checking-if-an-android-application-is-running-in-the-background
 */

public class ApplicationLifeCycleHandler implements Application.ActivityLifecycleCallbacks {

    // Using the approach defined here - http://steveliles.github.io/is_my_android_app_currently_foreground_or_background.html
    public interface Listener {
        void onBecameForeground();
        void onBecameBackground();
    }

    public static final long CHECK_DELAY = 500;
    public static final String TAG = ApplicationLifeCycleHandler.class.getName();

    private Handler handler = new Handler();
    private List<Listener> listeners = new CopyOnWriteArrayList<Listener>();
    private Runnable check;
    private volatile boolean foreground = false, isPaused = true;
    public void addListener(Listener listener){
        listeners.add(listener);
    }

    public void removeListener(Listener listener){
        listeners.remove(listener);
    }

    public ApplicationLifeCycleHandler(Application applicationContext, Listener sessionTracker) {
        super();
        // Resetting since static variables are not clear for forceful app exit.
        started = 0;
        resumed = 0;
        paused = 0;
        stopped = 0;
        addListener(sessionTracker);
        applicationContext.registerActivityLifecycleCallbacks(this);
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

    }

    @Override
    public void onActivityStarted(Activity activity) {
        started++;
    }

    @Override
    public void onActivityResumed(Activity activity) {
        resumed++;

        isPaused = false;
        boolean wasBackground = !foreground;
        foreground = true;
        if (check != null)
            handler.removeCallbacks(check);
        if (wasBackground){
            Log.i(TAG, "went foreground");
            for (Listener l : listeners) {
                try {
                    l.onBecameForeground();
                } catch (Exception exc) {
                    Log.e(TAG, "Listener threw exception!", exc);
                }
            }
            Intent intent = new Intent(QplumApplication.getQplumApplication(), TCPService.class);
            intent.setAction(TCPService.ACTION_START_SERVICE);
            QplumApplication.getQplumApplication().startService(intent);
        } else {
            Log.i(TAG, "still foreground");
        }
    }

    @Override
    public void onActivityPaused(Activity activity) {
        paused++;
        isPaused = true;

        if (check != null)
            handler.removeCallbacks(check);

        handler.postDelayed(check = new Runnable(){
            @Override
            public void run() {
                if (foreground && isPaused) {
                    foreground = false;
                    Log.i(TAG, "went background");
                    for (Listener l : listeners) {
                        try {
                            l.onBecameBackground();
                        } catch (Exception exc) {
                            Log.e(TAG, "Listener threw exception!", exc);
                        }
                    }
                } else {
                    Log.i(TAG, "still foreground");
                }
            }
        }, CHECK_DELAY);
    }

    @Override
    public void onActivityStopped(Activity activity) {
        stopped++;
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }

    // If you want a static function you can use to check if your application is
    // foreground/background, you can use the following:
    // Replace the four variables above with these four
    private int resumed;
    private int paused;
    private int started;
    private int stopped;

    // And these two public static functions
    public boolean isApplicationVisible() {
        return started > stopped;
    }

    public boolean isApplicationInForeground() {
        return resumed > paused;
    }

}
