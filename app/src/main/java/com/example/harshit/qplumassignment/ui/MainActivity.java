package com.example.harshit.qplumassignment.ui;

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.example.harshit.qplumassignment.NetworkConnectionIntentReceiver;
import com.example.harshit.qplumassignment.R;
import com.example.harshit.qplumassignment.TCPService;
import com.example.harshit.qplumassignment.TcpClient;
import com.example.harshit.qplumassignment.event.OnlineStatusEvent;
import com.example.harshit.qplumassignment.event.UpdateEvent;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.LargeValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import de.greenrobot.event.EventBus;

public class MainActivity extends AppCompatActivity implements OnChartValueSelectedListener, TcpClient.OnMessageReceived {

    private Toolbar toolbar;
    private CoordinatorLayout coordinatorLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        connect();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(getResources().getColor(R.color.atlas_background_blue));
        toolbar.setTitle(getResources().getString(R.string.qplum));
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }
        FloatingActionButton floatingActionButton = (FloatingActionButton) findViewById(R.id.fab);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, PurchaseActivity.class);
                startActivity(intent);
            }
        });
        setStatusBarColor();
        drawGraph();
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator_layout);
        snackbar = Snackbar.make(coordinatorLayout, getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE);
        if(!NetworkConnectionIntentReceiver.isOnline(this)) {
            onEventMainThread(new OnlineStatusEvent(false));
        }
    }

    private void connect() {
        Intent intent = new Intent(this, TCPService.class);
        intent.setAction(TCPService.ACTION_START_SERVICE);
        startService(intent);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void setStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            if (window != null) {
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(this.getResources().getColor(R.color.atlas_background_blue_dark));
            }
        }
    }

    private BarChart lastTradedPrice;
    private BarChart bestBuyPrice;
    private BarChart bestSellPrice;
    private Typeface tf;


    private void drawGraph() {

        lastTradedPrice = (BarChart) findViewById(R.id.chart1);
        bestBuyPrice = (BarChart) findViewById(R.id.chart2);
        bestSellPrice = (BarChart) findViewById(R.id.chart3);
        tf = Typeface.createFromAsset(getAssets(), "OpenSans-Regular.ttf");

        drawGraphInternal(lastTradedPrice);
        drawGraphInternal(bestBuyPrice);
        drawGraphInternal(bestSellPrice);

    }

    private void drawGraphInternal(BarChart barChart) {
        barChart.setOnChartValueSelectedListener(this);
        barChart.setDescription(getString(R.string.last_traded_price));


        // scaling can now only be done on x- and y-axis separately
        barChart.setPinchZoom(false);

        barChart.setDrawBarShadow(false);

        barChart.setDrawGridBackground(false);

        // create a custom MarkerView (extend MarkerView) and specify the layout
        // to use for it
        MarketViewImpl mv = new MarketViewImpl(this, R.layout.custom_marker_view);

        // set the marker to the chart
        barChart.setMarkerView(mv);



        Legend l = barChart.getLegend();
        l.setPosition(Legend.LegendPosition.RIGHT_OF_CHART_INSIDE);
        l.setTypeface(tf);
        l.setYOffset(0f);
        l.setYEntrySpace(0f);
        l.setTextSize(8f);

        XAxis xl = barChart.getXAxis();
        xl.setTypeface(tf);

        YAxis leftAxis = barChart.getAxisLeft();
        leftAxis.setTypeface(tf);
        leftAxis.setValueFormatter(new LargeValueFormatter());
        leftAxis.setDrawGridLines(false);
        leftAxis.setSpaceTop(30f);
        leftAxis.setAxisMinValue(40f); // this replaces setStartAtZero(true)
        leftAxis.setAxisMaxValue(50f);

        barChart.getAxisRight().setEnabled(false);
    }

    @Override
    public void onValueSelected(Entry entry, int i, Highlight highlight) {

    }

    @Override
    public void onNothingSelected() {

    }


    DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
    private void renderNewData() {
        ArrayList<BarEntry> lastTradedPrice = new ArrayList<>(TcpClient.noOfTicks);
        ArrayList<BarEntry> bestBuyPrice = new ArrayList<>(TcpClient.noOfTicks);
        ArrayList<BarEntry> bestSellPrice = new ArrayList<>(TcpClient.noOfTicks);
        ArrayList<String> timeStamp = new ArrayList<>(TcpClient.noOfTicks);

        for (int i = 0; i < TcpClient.getLast10Ticks().size(); i++) {
            TcpClient.StreamTick streamTick = TcpClient.getLast10Ticks().get(i);

            lastTradedPrice.add(new BarEntry(streamTick.lastTradedPrice, i));
            bestBuyPrice.add(new BarEntry(streamTick.bestBuyPrice, i));
            bestSellPrice.add(new BarEntry(streamTick.bestSellPrice, i));
            Date date = new Date(TcpClient.getLast10Ticks().get(i).timeStamp * 1000);
            timeStamp.add(dateFormat.format(date));
        }
        BarDataSet lastTradedPriceData = new BarDataSet(lastTradedPrice, getString(R.string.ltp));
        lastTradedPriceData.setColor(Color.rgb(104, 241, 175));
        ArrayList<IBarDataSet> lastTradedPriceDataSet = new ArrayList<>();

        lastTradedPriceDataSet.add(lastTradedPriceData);
        final BarData ltpDataSet = new BarData(timeStamp, lastTradedPriceDataSet);

        // add space between the dataset groups in percent of bar-width
        ltpDataSet.setGroupSpace(80f);
        ltpDataSet.setValueTypeface(tf);


        BarDataSet bestBuyPriceData = new BarDataSet(bestBuyPrice, getString(R.string.bbp));
        bestBuyPriceData.setColor(Color.rgb(242, 247, 158));
        ArrayList<IBarDataSet> bestBuyPriceDataSet = new ArrayList<>();

        bestBuyPriceDataSet.add(bestBuyPriceData);
        final BarData bpDataSet = new BarData(timeStamp, bestBuyPriceDataSet);

        // add space between the dataset groups in percent of bar-width
        bpDataSet.setGroupSpace(80f);
        bpDataSet.setValueTypeface(tf);

        BarDataSet bestSellPriceData = new BarDataSet(bestSellPrice, getString(R.string.bsp));
        bestSellPriceData.setColor(Color.rgb(164, 228, 251));
        ArrayList<IBarDataSet> bestSellPriceDataSet = new ArrayList<>();

        bestSellPriceDataSet.add(bestSellPriceData);
        final BarData bspDataSet = new BarData(timeStamp, bestSellPriceDataSet);

        // add space between the dataset groups in percent of bar-width
        bspDataSet.setGroupSpace(80f);
        bspDataSet.setValueTypeface(tf);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                MainActivity.this.lastTradedPrice.setData(ltpDataSet);
                MainActivity.this.bestBuyPrice.setData(bpDataSet);
                MainActivity.this.bestSellPrice.setData(bspDataSet);
                MainActivity.this.lastTradedPrice.invalidate();
                MainActivity.this.bestBuyPrice.invalidate();
                MainActivity.this.bestSellPrice.invalidate();
            }
        });
    }

    @Override
    public void messageReceived() {
        renderNewData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        TcpClient.addMessageListener(this);
    }

    @Override
    protected void onPause() {
        TcpClient.removeMessageListener();
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_history_overflow) {
            Intent intent = new Intent(this, HistoryActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onEventMainThread(UpdateEvent event) {
        Toast.makeText(this, event.message, Toast.LENGTH_LONG).show();
    }

    private Snackbar snackbar;
    public void onEventMainThread(OnlineStatusEvent event) {
        if (!event.isOnline) {

            snackbar.setAction(R.string.dismiss, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    snackbar.dismiss();
                }
            });

            snackbar.show();
        } else {
            snackbar.dismiss();
        }
    }
}
