package com.example.harshit.qplumassignment.event;

/**
 * Created by harshit on 21/4/16.
 */
public class OnlineStatusEvent {
    public OnlineStatusEvent(boolean isOnline) {
        this.isOnline = isOnline;
    }

    public boolean isOnline;
}
