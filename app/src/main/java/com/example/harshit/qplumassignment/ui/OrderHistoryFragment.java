package com.example.harshit.qplumassignment.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.harshit.qplumassignment.R;
import com.example.harshit.qplumassignment.helper.OrderHelper;
import com.example.harshit.qplumassignment.helper.TransactionHelper;
import com.example.harshit.qplumassignment.model.Order;
import com.example.harshit.qplumassignment.model.Transaction;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmBaseAdapter;
import io.realm.RealmResults;

/**
 * Created by harshit on 20/4/16.
 */
public class OrderHistoryFragment extends Fragment {

    private int position;
    private Realm realm;
    private RealmResults<Order> orders;
    private RealmResults<Transaction> transactions;
    public static Fragment init(int position) {
        OrderHistoryFragment orderHistoryFragment = new OrderHistoryFragment();
        orderHistoryFragment.position = position;
        return orderHistoryFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Realm.getInstance(getActivity());
        if (position == 0) {
            orders = OrderHelper.retrieveAllOrders(realm);
        } else {
            transactions = TransactionHelper.retrieveAllOrders(realm);
        }
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.history_fragment, null);
        ListView listView = (ListView) view.findViewById(R.id.history_view);
        if (position == 0) {
            OrderAdapter orderAdapter = new OrderAdapter(getActivity(), orders, true);
            listView.setAdapter(orderAdapter);
        } else  {
            TransactionAdapter transactionAdapter = new TransactionAdapter(getActivity(), transactions, true);
            listView.setAdapter(transactionAdapter);
        }
        return view;
    }

    class OrderAdapter extends RealmBaseAdapter<Order> {


        public OrderAdapter(Context context, RealmResults<Order> realmResults, boolean automaticUpdate) {
            super(context, realmResults, automaticUpdate);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            BaseOrderViewHolder baseOrderViewHolder;
            if (convertView == null) {
                baseOrderViewHolder = new BaseOrderViewHolder();
                convertView = inflater.inflate(R.layout.history_layout, null);
                baseOrderViewHolder.time = (TextView) convertView.findViewById(R.id.atlas_conversation_view_convert_time);
                baseOrderViewHolder.itemSpec = (TextView) convertView.findViewById(R.id.item_spec);
                baseOrderViewHolder.itemType = (TextView) convertView.findViewById(R.id.category);
                baseOrderViewHolder.pendingStatus = (ImageView) convertView.findViewById(R.id.background_view);
                convertView.setTag(baseOrderViewHolder);
            } else {
                baseOrderViewHolder = (BaseOrderViewHolder) convertView.getTag();
            }
            String isMarketOrdered = getItem(position).isMarketOrdered() ? context.getString(R.string.mo) : context.getString(R.string.lo);
            String isSold = "(" + (getItem(position).isSell() ? "Sell)" : "Purchased)");
            baseOrderViewHolder.itemType.setText(isMarketOrdered + isSold);
            if (isCompleted(getItem(position))) {
                baseOrderViewHolder.pendingStatus.setImageResource(R.drawable.completed);
            } else {
                baseOrderViewHolder.pendingStatus.setImageResource(R.drawable.pending);
            }
            baseOrderViewHolder.itemSpec.setText(generateString(getItem(position)));
            baseOrderViewHolder.time.setText(String.valueOf(formatTime(getItem(position).getTimeStamp())));
            return convertView;
        }

        private boolean isCompleted(Order item) {
            if (item.isMarketOrdered()) {
                if (item.getMarketOrder().getTransaction() != null) {
                    return true;
                }
            } else {
                if (item.getLimitOrder().getDesiredQuantity() == 0) {
                    return true;
                }
            }
            return false;
        }

        private String generateString(Order item) {
            if (item.isMarketOrdered()) {
                if (isCompleted(item)) {
                    return item.getMarketOrder().getTransaction().getNoOfQuantity() + " x " +
                            item.getMarketOrder().getTransaction().getPrice();
                } else {
                    return String.valueOf(item.getMarketOrder().getDesiredQuantity());
                }
            } else {
                return item.getLimitOrder().getDesiredQuantity() + " x " + item.getLimitOrder().getDesiredPrice();
            }
        }

    }

    class TransactionAdapter extends RealmBaseAdapter<Transaction> {

        public TransactionAdapter(Context context, RealmResults<Transaction> realmResults, boolean automaticUpdate) {
            super(context, realmResults, automaticUpdate);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            BaseOrderViewHolder baseOrderViewHolder;
            if (convertView == null) {
                baseOrderViewHolder = new BaseOrderViewHolder();
                convertView = inflater.inflate(R.layout.history_layout, null);
                baseOrderViewHolder.time = (TextView) convertView.findViewById(R.id.atlas_conversation_view_convert_time);
                baseOrderViewHolder.itemSpec = (TextView) convertView.findViewById(R.id.item_spec);
                baseOrderViewHolder.itemType = (TextView) convertView.findViewById(R.id.category);
                baseOrderViewHolder.pendingStatus = (ImageView) convertView.findViewById(R.id.background_view);
                convertView.setTag(baseOrderViewHolder);
            } else {
                baseOrderViewHolder = (BaseOrderViewHolder) convertView.getTag();
            }
            baseOrderViewHolder.itemSpec.setText(getItem(position).getNoOfQuantity() + " x " +
                    getItem(position).getPrice());
            baseOrderViewHolder.time.setText(String.valueOf(formatTime(getItem(position).getTimeStamp())));
            baseOrderViewHolder.itemType.setVisibility(View.GONE);
            baseOrderViewHolder.pendingStatus.setImageResource(R.drawable.completed);
            return convertView;
        }
    }

    private static class BaseOrderViewHolder {
        private ImageView pendingStatus;
        private TextView itemSpec;
        private TextView itemType;
        private TextView time;
    }

    @Override
    public void onDestroy() {
        realm.close();
        super.onDestroy();
    }

    static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static String formatTime(long timeStamp) {
        Date date = new Date(timeStamp * 1000);
        return dateFormat.format(date);
    }
}
