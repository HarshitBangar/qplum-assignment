package com.example.harshit.qplumassignment.event;

/**
 * Created by harshit on 21/4/16.
 */
public class UpdateEvent {
    public UpdateEvent(String message) {
        this.message = message;
    }

    public String message;
}
