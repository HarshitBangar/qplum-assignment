package com.example.harshit.qplumassignment.model;

import io.realm.RealmObject;

/**
 * Created by harshit on 20/4/16.
 */
public class MarketOrder extends RealmObject {
    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public int getDesiredQuantity() {
        return desiredQuantity;
    }

    public void setDesiredQuantity(int desiredQuantity) {
        this.desiredQuantity = desiredQuantity;
    }

    private Transaction transaction;
    private int desiredQuantity;
}
