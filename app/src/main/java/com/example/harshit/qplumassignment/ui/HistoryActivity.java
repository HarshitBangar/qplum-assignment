package com.example.harshit.qplumassignment.ui;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.example.harshit.qplumassignment.R;
import com.example.harshit.qplumassignment.event.UpdateEvent;

/**
 * Created by harshit on 20/4/16.
 */
public class HistoryActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager mViewPager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        HistoryPagerAdapter historyPagerAdapter = new HistoryPagerAdapter(getSupportFragmentManager());
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        toolbar.setBackgroundColor(getResources().getColor(R.color.atlas_background_blue));
        toolbar.setTitle(getResources().getString(R.string.qplum));
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        setStatusBarColor();
        mViewPager = (ViewPager) findViewById(R.id.pager);
        if (ViewCompat.isLaidOut(tabLayout)) {
            tabLayout.setupWithViewPager(mViewPager);
            tabLayout.setOnTabSelectedListener(tabSelectedListener());
        } else {
            tabLayout.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                @Override
                public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                    tabLayout.setupWithViewPager(mViewPager);
                    tabLayout.removeOnLayoutChangeListener(this);
                    tabLayout.setOnTabSelectedListener(tabSelectedListener());
                }
            });
        }
        mViewPager.setAdapter(historyPagerAdapter);
    }

    private TabLayout.OnTabSelectedListener tabSelectedListener() {
        return new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        };
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void setStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            if (window != null) {
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(this.getResources().getColor(R.color.atlas_background_blue_dark));
            }
        }
    }

    class HistoryPagerAdapter extends FragmentPagerAdapter {

        public HistoryPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = OrderHistoryFragment.init(position);
            return fragment;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (position == 0) {
                return getString(R.string.orders);
            } else {
                return getString(R.string.transactions);
            }
        }
    }

    public void onEventMainThread(UpdateEvent event) {
        Toast.makeText(this, event.message, Toast.LENGTH_LONG).show();
    }

}
