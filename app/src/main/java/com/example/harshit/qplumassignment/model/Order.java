package com.example.harshit.qplumassignment.model;

import io.realm.RealmObject;

/**
 * Created by harshit on 20/4/16.
 */
public class Order extends RealmObject {

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public boolean isSell() {
        return sell;
    }

    public void setSell(boolean sell) {
        this.sell = sell;
    }

    public boolean isMarketOrdered() {
        return isMarketOrdered;
    }

    public void setMarketOrdered(boolean marketOrdered) {
        this.isMarketOrdered = marketOrdered;
    }

    public MarketOrder getMarketOrder() {
        return marketOrder;
    }

    public void setMarketOrder(MarketOrder marketOrder) {
        this.marketOrder = marketOrder;
    }

    public LimitOrder getLimitOrder() {
        return limitOrder;
    }

    public void setLimitOrder(LimitOrder limitOrder) {
        this.limitOrder = limitOrder;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    private String orderId;
    private boolean sell;
    private boolean isMarketOrdered;
    private MarketOrder marketOrder;
    private LimitOrder limitOrder;
    private long timeStamp;
}
