package com.example.harshit.qplumassignment.ui;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.harshit.qplumassignment.R;
import com.example.harshit.qplumassignment.TcpClient;
import com.example.harshit.qplumassignment.event.UpdateEvent;
import com.example.harshit.qplumassignment.helper.OrderHelper;
import com.example.harshit.qplumassignment.helper.TransactionHelper;
import com.liuguangqiang.materialdialog.MaterialDialog;

import java.util.Arrays;
import java.util.List;

import io.realm.Realm;

/**
 * Created by harshit on 20/4/16.
 */
public class PurchaseActivity extends AppCompatActivity {

    TextView type;
    TextView sellBuy;
    EditText price;
    EditText quantity;
    Realm realm;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Realm.getInstance(this);
        setContentView(R.layout.purchase_screen);
        type = (TextView) findViewById(R.id.goc_name);
        sellBuy = (TextView) findViewById(R.id.goc_category_name);
        price = (EditText) findViewById(R.id.title);
        quantity = (EditText) findViewById(R.id.description);
        if (savedInstanceState != null) {
            quantity.setText(savedInstanceState.getString(quantityContainer));
            price.setText(savedInstanceState.getString(priceContainer));
            sellBuy.setText(savedInstanceState.getString(sellBuyContainer));
            type.setText(savedInstanceState.getString(typeContainer));
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.qplum);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setStatusBarColor();
        findViewById(R.id.goc_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTypeDialog();
            }
        });
        findViewById(R.id.subcategory).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showBuySellDialog();
            }
        });
        findViewById(R.id.fabbutton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String orderType = type.getText().toString();
                if (TextUtils.isEmpty(orderType)) {
                    Toast.makeText(PurchaseActivity.this, R.string.eq, Toast.LENGTH_SHORT).show();
                    return;
                }
                if (marketOrder.equals(orderType)) {
                    if (TextUtils.isEmpty(quantity.getText())
                            || TextUtils.isEmpty(sellBuy.getText())) {
                        Toast.makeText(PurchaseActivity.this, R.string.eq, Toast.LENGTH_SHORT).show();
                        return;

                    }
                    int quantityVal = Integer.valueOf(quantity.getText().toString());
                    boolean sell = !buy.equals(sellBuy.getText().toString());
                    TcpClient.StreamTick latestTick = TcpClient.getLatestTick();
                    String orderId = OrderHelper.createMarketOrder(realm, quantityVal, sell, latestTick.timeStamp);

                    if (sell) {
                        if (latestTick.bestSelllQuantity < quantityVal) {
                            TransactionHelper.createTransaction(realm, latestTick.bestSelllQuantity, latestTick.bestSellPrice,
                                    latestTick.timeStamp, orderId);
                            Toast.makeText(PurchaseActivity.this, latestTick.bestSelllQuantity + " sold at " +
                                    latestTick.bestSellPrice, Toast.LENGTH_LONG).show();
                        } else {
                            TransactionHelper.createTransaction(realm, quantityVal, latestTick.bestSellPrice,
                                    latestTick.timeStamp, orderId);
                        }
                    } else {
                        if (latestTick.bestBuyQuantity < quantityVal) {
                            TransactionHelper.createTransaction(realm, latestTick.bestBuyQuantity, latestTick.bestBuyPrice,
                                    latestTick.timeStamp, orderId);
                            Toast.makeText(PurchaseActivity.this, latestTick.bestBuyQuantity + " bought at " +
                                    latestTick.bestBuyPrice, Toast.LENGTH_LONG).show();
                        } else {
                            TransactionHelper.createTransaction(realm, quantityVal, latestTick.bestBuyPrice,
                                    latestTick.timeStamp, orderId);
                        }
                    }
                    finish();
                    return;
                } else if (limitOrder.equals(orderType)) {
                    if (TextUtils.isEmpty(quantity.getText())
                            || TextUtils.isEmpty(price.getText())
                            || TextUtils.isEmpty(sellBuy.getText())) {
                        Toast.makeText(PurchaseActivity.this, R.string.eq, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    int quantityVal = Integer.valueOf(quantity.getText().toString());
                    boolean sell = !Boolean.valueOf(buy.equals(sellBuy.getText().toString()));
                    double priceVal = Double.valueOf(price.getText().toString());
                    OrderHelper.createLimitOrder(realm, quantityVal,
                            sell, System.currentTimeMillis()/1000, priceVal);
                    finish();
                    return;
                }
                Toast.makeText(PurchaseActivity.this, R.string.eq, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void setStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            if (window != null) {
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(this.getResources().getColor(R.color.atlas_background_blue_dark));
            }
        }
    }

    private String marketOrder = "MARKET ORDER";
    private String limitOrder = "LIMIT ORDER";
    private String sell = "Sell";
    private String buy = "Buy";

    private void showTypeDialog() {
        String[] categoryArray = getResources().getStringArray(R.array.typeList);
        final List<String> categoryList = Arrays.asList(categoryArray);
        new MaterialDialog.Builder(this)
                .setTitle(R.string.category)
                .setItems(categoryArray, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        String category = categoryList.get(which);
                        type.setText(category);
                        if (marketOrder.equals(category)) {
                            findViewById(R.id.description_container).setVisibility(View.GONE);
                        } else if (limitOrder.equals(category)) {
                            findViewById(R.id.description_container).setVisibility(View.VISIBLE);
                        }
                    }
                })
                .build()
                .show();
    }

    private void showBuySellDialog() {
        String[] categoryArray = getResources().getStringArray(R.array.buysell);
        final List<String> categoryList = Arrays.asList(categoryArray);
        new MaterialDialog.Builder(this)
                .setTitle(R.string.subcategory)
                .setItems(categoryArray, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        String subCategory = categoryList.get(which);
                        sellBuy.setText(subCategory);
                    }
                })
                .build()
                .show();
    }

    @Override
    protected void onDestroy() {
        realm.close();
        super.onDestroy();
    }

    public void onEventMainThread(UpdateEvent event) {
        Toast.makeText(this, event.message, Toast.LENGTH_LONG).show();
    }

    private static final String typeContainer = "TYPE";
    private static final String sellBuyContainer = "SELL";
    private static final String priceContainer = "PRICE";
    private static final String quantityContainer = "QUANTITY";
    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        // Save custom values into the bundle
        savedInstanceState.putString(typeContainer, type.getText().toString());
        savedInstanceState.putString(sell, sellBuy.getText().toString());
        savedInstanceState.putString(priceContainer, price.getText().toString());
        savedInstanceState.putString(quantityContainer, quantity.getText().toString());
        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }
}
