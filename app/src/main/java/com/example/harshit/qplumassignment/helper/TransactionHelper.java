package com.example.harshit.qplumassignment.helper;

import com.example.harshit.qplumassignment.model.MarketOrder;
import com.example.harshit.qplumassignment.model.Order;
import com.example.harshit.qplumassignment.model.Transaction;

import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by harshit on 20/4/16.
 */
public class TransactionHelper {

    public static RealmResults<Transaction> retrieveAllOrders(Realm realm) {
        RealmResults<Transaction> transactions = realm.where(Transaction.class).findAll();
        transactions.sort("timeStamp", Sort.DESCENDING);
        return transactions;
    }

    public static void createTransaction(Realm realm, int quantityVal,
                                           double price, long timeStamp, String orderId) {
        String id = UUID.randomUUID().toString();
        Order order = realm.where(Order.class).equalTo("orderId", orderId).findFirst();
        realm.beginTransaction();
        try {
            Transaction transaction = realm.createObject(Transaction.class);
            transaction.setTimeStamp(timeStamp);
            transaction.setId(id);
            transaction.setNoOfQuantity(quantityVal);
            transaction.setPrice(price);
            if (order.isMarketOrdered()) {
                order.getMarketOrder().setTransaction(transaction);
            } else {
                order.getLimitOrder().setDesiredQuantity(order.getLimitOrder().getDesiredQuantity() - quantityVal);
                order.getLimitOrder().getTransactions().add(transaction);
            }
            realm.commitTransaction();
        } catch (Exception ex) {
            realm.cancelTransaction();
        }
    }
}
