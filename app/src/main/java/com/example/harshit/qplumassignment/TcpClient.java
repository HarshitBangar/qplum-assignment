package com.example.harshit.qplumassignment;

/**
 * Created by harshit on 18/4/16.
 */

import android.util.Log;

import com.example.harshit.qplumassignment.event.UpdateEvent;
import com.example.harshit.qplumassignment.helper.OrderHelper;
import com.example.harshit.qplumassignment.helper.TransactionHelper;
import com.example.harshit.qplumassignment.model.Order;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import de.greenrobot.event.EventBus;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Description
 *
 * @author Harshit Bangar
 */
public class TcpClient {

    public static final String SERVER_IP = "52.77.239.86";
    public static final int SERVER_PORT = 48129;
    // sends message received notifications
    private static OnMessageReceived mMessageListener = null;
    // while this is true, the server will continue running
    private volatile boolean mRun = false;
    // used to send messages
    private PrintWriter mBufferOut;

    private static StreamTick latestTick;

    public static void addTick(StreamTick tick) {
        last10Ticks.add(tick);
        latestTick = tick;
        if (last10Ticks.size() > noOfTicks) {
            last10Ticks.subList(0, last10Ticks.size() -noOfTicks).clear();
        }
        if (mMessageListener != null) {
            mMessageListener.messageReceived();
        }
        verifyPendingOrders(tick);
    }



    private static void verifyPendingOrders(StreamTick tick) {
        for (Order order : pendingOrders) {
            if (order.isSell()) {
                if (order.getLimitOrder().getDesiredPrice() < tick.bestSellPrice) {
                    Realm realm = Realm.getInstance(QplumApplication.getQplumApplication());
                    int quantityPurchased = tick.bestSelllQuantity > order.getLimitOrder().getDesiredQuantity() ? order.getLimitOrder().getDesiredQuantity():tick.bestSelllQuantity;
                    TransactionHelper.createTransaction(realm, quantityPurchased, tick.bestSellPrice, tick.timeStamp, order.getOrderId());
                    realm.close();
                    populatePendingOrders();
                    EventBus.getDefault().post(new UpdateEvent("Order with details:" + order.getLimitOrder().getDesiredPrice() + ": " + quantityPurchased + " has been sold." ));
                }
            } else {
                if (order.getLimitOrder().getDesiredPrice() > tick.bestBuyPrice) {
                    Realm realm = Realm.getInstance(QplumApplication.getQplumApplication());
                    int quantitySold = tick.bestBuyQuantity > order.getLimitOrder().getDesiredQuantity() ? order.getLimitOrder().getDesiredQuantity():tick.bestBuyQuantity;
                    TransactionHelper.createTransaction(realm, quantitySold, tick.bestBuyPrice, tick.timeStamp, order.getOrderId());
                    realm.close();
                    populatePendingOrders();
                    EventBus.getDefault().post(new UpdateEvent("Order with details:" + order.getLimitOrder().getDesiredPrice() + ": " + quantitySold + " has been purchased." ));
                }
            }
        }
    }

    public static StreamTick getLatestTick() {
        return latestTick;
    }

    public static int noOfTicks = 4;

    public static List<StreamTick> getLast10Ticks() {
        return last10Ticks;
    }

    private static List<StreamTick> last10Ticks = new CopyOnWriteArrayList<>();

    /**
     * Constructor of the class. OnMessagedReceived listens for the messages received from server
     */
    public TcpClient() {
        populatePendingOrders();
    }

    public static void addMessageListener(OnMessageReceived onMessageReceivedIn) {
        mMessageListener = onMessageReceivedIn;
    }

    public static void removeMessageListener() {
        mMessageListener = null;
    }

    /**
     * Sends the message entered by client to the server
     *
     * @param message text entered by client
     */
    public void sendMessage(String message) {
        if (mBufferOut != null && !mBufferOut.checkError()) {
            mBufferOut.println(message);
            mBufferOut.flush();
        }
    }

    /**
     * Close the connection and release the members
     */
    public void stopClient() {
        Log.i("Debug", "stopClient");

        // send mesage that we are closing the connection
        //sendMessage(Constants.CLOSED_CONNECTION + "Kazy");

        mRun = false;
        if (mBufferOut != null) {
            mBufferOut.flush();
            mBufferOut.close();
        }
        mBufferOut = null;
    }

    public void onClose() {
        mMessageListener = null;
    }

    public void run() {

        if (mRun) {
            return;
        }
        mRun = true;

        try {
            //here you must put your computer's IP address.
            Log.e("TCP Client", "C: Connecting...");

            //create a socket to make the connection with the server
            Socket s = new Socket("52.77.239.86", 48129);

            BufferedReader r = new BufferedReader(new InputStreamReader(
                    s.getInputStream()));

            PrintStream w = new PrintStream(s.getOutputStream());

            w.print("hello world");
            w.print("\n"); // enter new line
            w.flush();// flush the outputstream
            String line;

            while (mRun) {
                line = r.readLine();
                StreamTick streamTick = StreamTick.fromString(line);
                Log.d("Received: ", streamTick.toString());
                addTick(streamTick);
            }
            w.close();

        } catch (Exception e) {

            Log.e("TCP", "C: Error", e);
            mRun = false;

        }

    }

    //Declare the interface. The method messageReceived(String message) will must be implemented in the MyActivity
    //class at on asynckTask doInBackground
    public interface OnMessageReceived {
        void messageReceived();
    }

    public static class StreamTick {
        public long timeStamp;
        public float lastTradedPrice;
        public float bestBuyPrice;
        public int bestBuyQuantity;
        public float bestSellPrice;
        public int bestSelllQuantity;

        private static StreamTick fromString(String input) {
            String[] tickComponent = input.split(",");
            if (tickComponent.length != 6) {
                return null;
            }
            StreamTick streamTick = new StreamTick();
            streamTick.timeStamp = Long.valueOf(tickComponent[0].trim());
            streamTick.lastTradedPrice = Float.valueOf(tickComponent[1].trim());
            streamTick.bestBuyPrice = Float.valueOf(tickComponent[2].trim());
            streamTick.bestBuyQuantity = Integer.valueOf(tickComponent[3].trim());
            streamTick.bestSellPrice = Float.valueOf(tickComponent[4].trim());
            streamTick.bestSelllQuantity = Integer.valueOf(tickComponent[5].trim());
            return streamTick;
        }

        @Override
        public String toString() {
            return "StreamTick{" +
                    "timeStamp=" + timeStamp +
                    ", lastTradedPrice=" + lastTradedPrice +
                    ", bestBuyPrice=" + bestBuyPrice +
                    ", bestBuyQuantity=" + bestBuyQuantity +
                    ", bestSellPrice=" + bestSellPrice +
                    ", bestSelllQuantity=" + bestSelllQuantity +
                    '}';
        }
    }

    private static List<Order> pendingOrders = new CopyOnWriteArrayList<>();

    public static void populatePendingOrders() {
        Realm realm = Realm.getInstance(QplumApplication.getQplumApplication().getApplicationContext());
        RealmResults<Order> orders = OrderHelper.retrievePendingLimitOrder(realm);
        pendingOrders = realm.copyFromRealm(orders);
        realm.close();
    }
}
