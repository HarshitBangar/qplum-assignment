package com.example.harshit.qplumassignment;

import android.app.Application;

/**
 * Created by harshit on 20/4/16.
 */
public class QplumApplication extends Application implements ApplicationLifeCycleHandler.Listener {

    @Override
    public void onCreate() {
        super.onCreate();
        qplumApplication = this;
        applicationLifeCycleHandler = new ApplicationLifeCycleHandler(this, this);
    }

    public static QplumApplication getQplumApplication() {
        return qplumApplication;
    }

    public static ApplicationLifeCycleHandler getApplicationLifeCycleHandler() {
        return applicationLifeCycleHandler;
    }

    private static QplumApplication qplumApplication;
    private static ApplicationLifeCycleHandler applicationLifeCycleHandler;

    @Override
    public void onBecameForeground() {

    }

    @Override
    public void onBecameBackground() {

    }

}
