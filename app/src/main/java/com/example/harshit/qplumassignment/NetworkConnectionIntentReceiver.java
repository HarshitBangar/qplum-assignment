package com.example.harshit.qplumassignment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.example.harshit.qplumassignment.event.OnlineStatusEvent;

import de.greenrobot.event.EventBus;

/**
 * Created by harshit on 20/4/16.
 */
public class NetworkConnectionIntentReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        if (!QplumApplication.getApplicationLifeCycleHandler().isApplicationInForeground()) {
            return;
        }
        if (isOnline(context)) {
            EventBus.getDefault().post(new OnlineStatusEvent(true));
            Intent intent1 = new Intent(context, TCPService.class);
            intent1.setAction(TCPService.ACTION_START_SERVICE);
            context.startService(intent1);
        } else {
            EventBus.getDefault().post(new OnlineStatusEvent(false));
            Intent intent1 = new Intent(context, TCPService.class);
            intent1.setAction(TCPService.ACTION_DISCONNECT_SERVICE);
            context.startService(intent1);
        }

    }

    /**
     * @return whether the android service can be regarded as online
     */
    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
        if (cm != null) {
            NetworkInfo networkInfo = cm.getActiveNetworkInfo();
            if (networkInfo != null &&
                    networkInfo.isAvailable() && networkInfo.isConnected()) {
                return true;
            }
        }
        return false;
    }
}
