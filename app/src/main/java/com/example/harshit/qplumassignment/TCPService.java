package com.example.harshit.qplumassignment;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.example.harshit.qplumassignment.helper.OrderHelper;
import com.example.harshit.qplumassignment.model.Order;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;


public class TCPService extends Service implements ApplicationLifeCycleHandler.Listener {
    public static final String ACTION_START_SERVICE = "ACTION_START_SERVICE";
    public static final String ACTION_DISCONNECT_SERVICE = "ACTION_DISCONNECT_SERVICE";
    private TcpClient tcpClient;
    private Looper mServiceLooper;
    public TCPService() {
    }

    @Override
    public void onBecameForeground() {

    }

    @Override
    public void onBecameBackground() {
        stopSelf();
    }

    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            onHandleIntent((Intent) msg.obj);
        }
    }

    private void onHandleIntent(Intent obj) {
        if (obj == null || obj.getAction() == null) {
            obj = new Intent(ACTION_START_SERVICE);
        }
        switch (obj.getAction()) {
            case ACTION_START_SERVICE:
                if (tcpClient == null) {
                    tcpClient = new TcpClient();
                }
                tcpClient.run();
                break;
            case ACTION_DISCONNECT_SERVICE:
                if (tcpClient == null) {
                    return;
                }
                tcpClient.stopClient();
                break;
        }

    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            Log.d("onStartCommand", "issuing START_SERVICE");
            intent = new Intent(ACTION_START_SERVICE);
        }
        performInServiceHandler(intent);
        return START_STICKY;
    }

    public void performInServiceHandler(Intent intent) {
        Message msg = mServiceHandler.obtainMessage();
        msg.obj = intent;
        msg.what = intent.getAction().hashCode();
        mServiceHandler.sendMessage(msg);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        String mName = TCPService.class.getSimpleName();
        final String threadName = "MAXSTransportService[" + mName + "]";
        HandlerThread thread = new HandlerThread(threadName);
        thread.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable ex) {
                Log.e(threadName, " thread: uncaught Exception!", ex);
            }
        });
        thread.start();

        mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);
        QplumApplication.getApplicationLifeCycleHandler().addListener(this);

    }

    @Override
    public void onDestroy() {
        mServiceLooper.quit();
        if(tcpClient != null) {
            tcpClient.stopClient();
            tcpClient.onClose();
        }
        tcpClient = null;
        QplumApplication.getApplicationLifeCycleHandler().removeListener(this);
        super.onDestroy();
    }

    private volatile ServiceHandler mServiceHandler;
}
