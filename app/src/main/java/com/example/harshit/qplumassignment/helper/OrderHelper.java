package com.example.harshit.qplumassignment.helper;

import com.example.harshit.qplumassignment.TcpClient;
import com.example.harshit.qplumassignment.model.LimitOrder;
import com.example.harshit.qplumassignment.model.MarketOrder;
import com.example.harshit.qplumassignment.model.Order;
import com.example.harshit.qplumassignment.model.Transaction;

import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by harshit on 20/4/16.
 */
public class OrderHelper {

    public static RealmResults<Order> retrieveAllOrders(Realm realm) {
        RealmResults<Order> orders = realm.where(Order.class).findAll();
        orders.sort("timeStamp", Sort.DESCENDING);
        return orders;
    }

    public static RealmResults<Order> retrievePendingLimitOrder(Realm realm) {
        return realm.where(Order.class).equalTo("isMarketOrdered", false).
                greaterThan("limitOrder.desiredQuantity", 0).findAll();
    }

    public static String createMarketOrder(Realm realm, int quantityVal, boolean sell, long timeStamp) {
        String orderId = UUID.randomUUID().toString();
        realm.beginTransaction();
        try {
            MarketOrder marketOrder = realm.createObject(MarketOrder.class);
            marketOrder.setDesiredQuantity(quantityVal);
            marketOrder.setTransaction(null);
            Order order = realm.createObject(Order.class);
            order.setLimitOrder(null);
            order.setMarketOrder(marketOrder);
            order.setMarketOrdered(true);
            order.setOrderId(orderId);
            order.setSell(sell);
            order.setTimeStamp(timeStamp);
            realm.commitTransaction();
            return orderId;
        } catch (Exception ex) {
            realm.cancelTransaction();
        }
        return null;
    }

    public static String createLimitOrder(Realm realm, int quantityVal, boolean sell, long timeStamp, double price) {
        String orderId = UUID.randomUUID().toString();
        realm.beginTransaction();
        try {
            LimitOrder limitOrder = realm.createObject(LimitOrder.class);
            limitOrder.setDesiredQuantity(quantityVal);
            limitOrder.setDesiredPrice(price);
            limitOrder.setTransactions(new RealmList<Transaction>());
            Order order = realm.createObject(Order.class);
            order.setLimitOrder(limitOrder);
            order.setMarketOrder(null);
            order.setMarketOrdered(false);
            order.setOrderId(orderId);
            order.setSell(sell);
            order.setTimeStamp(timeStamp);
            realm.commitTransaction();
            TcpClient.populatePendingOrders();
            return orderId;
        } catch (Exception ex) {
            realm.cancelTransaction();
        }
        return null;
    }
}
