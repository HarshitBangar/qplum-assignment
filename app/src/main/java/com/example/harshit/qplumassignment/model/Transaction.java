package com.example.harshit.qplumassignment.model;

import io.realm.RealmObject;

/**
 * Created by harshit on 20/4/16.
 */
public class Transaction extends RealmObject {
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getNoOfQuantity() {
        return noOfQuantity;
    }

    public void setNoOfQuantity(int noOfQuantity) {
        this.noOfQuantity = noOfQuantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    private String id;
    private int noOfQuantity;
    private double price;
    private long timeStamp;
}
