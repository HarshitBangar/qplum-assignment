package com.example.harshit.qplumassignment.model;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by harshit on 20/4/16.
 */
public class LimitOrder extends RealmObject {
    public RealmList<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(RealmList<Transaction> transactions) {
        this.transactions = transactions;
    }

    public int getDesiredQuantity() {
        return desiredQuantity;
    }

    public void setDesiredQuantity(int desiredQuantity) {
        this.desiredQuantity = desiredQuantity;
    }

    public double getDesiredPrice() {
        return desiredPrice;
    }

    public void setDesiredPrice(double desiredPrice) {
        this.desiredPrice = desiredPrice;
    }

    private RealmList<Transaction> transactions;
    private int desiredQuantity;
    private double desiredPrice;
}
